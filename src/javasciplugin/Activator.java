package javasciplugin;


import java.util.List;
import java.util.Vector;

import javasciplugin.console.myEditorInput;

import org.eclipse.core.runtime.IPath;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.ide.IDE;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.osgi.framework.BundleContext;

/**
 * The activator class controls the plug-in life cycle. The start method is called when any part of the plugin is started. 
 * It will run until the plugin is closed (when eclipse closes).
 * The activator is responsible to initialise the plugin. This will load the JavaSciWrapper and the paths to plugin internal files are said.
 */
public class Activator extends AbstractUIPlugin {
	
	private static JavaSciWrapper myWrapper;
	private static IPath tempFile;
	private static IPath historyFile;
	private boolean isLoading = false;
	private List<WrapperReceiver> receivers = new Vector<WrapperReceiver>(); 

	public static final String PLUGIN_ID = "JavaSciPlugin";
	private static Activator plugin;
	
	/**
	 * This is the constructor of the activator
	 * **/
	public Activator() {
		super();
		plugin = this;
	}
	
	/**
	 * This method creates and initialises the Javasciwrapper and sends it to the views and editors that have requested it.
	 * The initialisations and constructions are done in a separate thread. This is done to prevent the views an editors from blocking until the initialisation is done.
	 * After the initialisation is finished the WrapperReceivers that requested the wrapper are notified. This method is only called when the plugin is started.
	 * @param r is an instance of a class that implements the WrapperReceiver interface and that requests the JavaSciWrapper(HistoryWidget, WorkspaceWidget ).
	 * **/
	public synchronized void getWrapper(WrapperReceiver r) {
		if (isLoading) {
			receivers.add(r);
			return;
		}
		if (myWrapper == null) {
			isLoading = true;
			receivers.add(r);
			new Thread() {
				public void run() {
					synchronized(Activator.this) {
						myWrapper=new JavaSciWrapper();
						myWrapper.loadSciHistory();
						myWrapper.loadSciWorkspace();
						for (int i = 0;i < receivers.size();i++) {
							receivers.get(i).receiveWrapper(myWrapper);
						}
						receivers.clear();
						isLoading = false;						
					}
				}
			}.start();
		} else {
			r.receiveWrapper(myWrapper);			
		}
	}
	
	/**
	 * This method returns the path to the temporary file. This file is used to generate a temporary Scilab script from a multi lined command.
	 * The file is located in the plugins working directory.
	 * @return the path to the temporary file.
	 * **/
	public IPath getTempFile(){
		if(tempFile==null){
			tempFile=this.getStateLocation().append("temp.sce");
		}
		return tempFile;
	}
	
	/**
	 * This method returns the path to the history file. This file is used to store all the commands that where entered in the Scilab plugin because the plugin has no write access to the Scilab history.
	 * The file is located in the plugins working directory.
	 * @return the path to the history file.
	 * **/
	public IPath getHistoryFile(){
		if(historyFile==null){
			historyFile=this.getStateLocation().append("history.txt");
		}
		return historyFile;
	}
	
	/**
	 * This method starts the console or brings it to the front of eclipse.
	 * **/
	public synchronized void startConsole(){		
		try {
			myEditorInput input=new myEditorInput();
			IDE.openEditor(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage(), input,"javasciplugin.console.SciConsole");
		} catch (PartInitException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * This method initialises the temporary file path and the history file path. Furthermore the BundleContext is passed to the plugin.
	 * @param context BundleContext
	 * **/
	public void start(BundleContext context) throws Exception {
		super.start(context);
		tempFile=this.getStateLocation().append("temp.sce");
		historyFile=this.getStateLocation().append("history.txt");
	}
	
	/**
	 * This method disposes javaSciWrapper. Furthermore the BundleContext is used to stop the plugin.
	 * @param context BundleContext
	 * **/
	public void stop(BundleContext context) throws Exception {
		myWrapper.dispose();
		super.stop(context);
	}

	/**
	 * This method returns the Activator of this plugin.
	 * @return Activator
	 * **/
	public synchronized static Activator getDefault() {
		if(plugin==null){
			plugin = new Activator();
		}
		return plugin;
	}

}
