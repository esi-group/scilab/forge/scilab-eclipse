package javasciplugin;

import org.eclipse.ui.IFolderLayout;
import org.eclipse.ui.IPageLayout;
import org.eclipse.ui.IPerspectiveFactory;


/**
 * This class is used to describe the Scilab perspective. A perspective is a combination of different views and editors that are used together.
 * In our case the Scilab history, workspace, file browser and the console form this perspective.
 * **/
public class SciPerspectiveFactory implements IPerspectiveFactory {

	/**
	 * This method is called when the perspective is opened. It indicates where and how the different views should be shown.
	 * @param layout is the layout of one eclipse workbench page.
	 * **/
	public void createInitialLayout(IPageLayout layout) {
		
		String editorArea = layout.getEditorArea();
		
		IFolderLayout left = layout.createFolder("left", IPageLayout.LEFT, (float) 0.25, editorArea);
		left.addView("javasciplugin.view.SciHistory");

		IFolderLayout right = layout.createFolder("right", IPageLayout.RIGHT, (float) 0.75, editorArea);
		right.addView("javasciplugin.workspace.SciWorkspace");
		right.addView("javasciplugin.fileBrowser.SciFileBrowser");
	}
}


