package javasciplugin.console;


import java.util.Vector;
import javasciplugin.Activator;
import javasciplugin.JavaSciWrapper;
import javasciplugin.WrapperReceiver;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StyleRange;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.dnd.DND;
import org.eclipse.swt.dnd.DropTarget;
import org.eclipse.swt.dnd.DropTargetAdapter;
import org.eclipse.swt.dnd.DropTargetEvent;
import org.eclipse.swt.dnd.FileTransfer;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.swt.events.*;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.*;

/**
 * This class defines a SWT Widget that can be loaded as a part of a eclipse plugin.
 * It defines layout and the events of the Scilab console. It is composed of two SWT Texts which are placed on a GridLayout.
 * **/
public class ConsoleWidget extends Composite implements WrapperReceiver{

	private Vector outputVector;
	private int historyIndex;
	private String commandBeginning;
	private String command;
	private JavaSciWrapper wrapper;
	private Text textinput;
	private StyledText textoutput;
	private Color myColor;
	private Color myRed;
	
	/**
	 * This constructor initialises the SWT components and puts them on a GridLayout.
	 * Furthermore a key listener, a traverse listener and a drop listener are added to the textInput Text.
	 * The key listener is used to cycle through the history when the up or down arrow key is pressed. 
	 * If a text was entered only the history elements that start with this text are shown.
	 * The traverse listener reacts to the return key and executes the command that is in the textInput.
	 * The textInput and the search text of the arrow keys are reseted and the output of the execution is shown in the textOutput.
	 * The drop listener is used to dorp the file path into the textInput. This can be used to drop scripts into the console and execute them. 
	 * In the end the javaSciWrapper is requested from the Activator as every execution needs to pass through the JavaSciWrapper.
	 * @param parent is used to register the widget in the eclipse engine.
	 * @param style is used to register the widget in the eclipse engine.
	 * **/
	ConsoleWidget(Composite parent, int style) {
		
		super(parent, style);
		textoutput = new StyledText(this, SWT.MULTI | SWT.V_SCROLL |   SWT.H_SCROLL | SWT.BORDER |SWT.READ_ONLY);
		GridData data=new GridData(GridData.FILL_BOTH);
		historyIndex=0;
		myColor = this.getDisplay().getSystemColor(SWT.COLOR_BLUE);
		myRed = this.getDisplay().getSystemColor(SWT.COLOR_RED);
		textoutput.setLayoutData(data);
		Font myFont= new Font(this.getParent().getDisplay(), "Courier New", 8, SWT.NORMAL);
		textoutput.setFont(myFont);
		textinput = new Text(this, SWT.MULTI | SWT.V_SCROLL |   SWT.H_SCROLL | SWT.BORDER);
		data=new GridData(GridData.FILL_BOTH);
		textinput.setLayoutData(data);
		
		Activator.getDefault().getWrapper(this);
		
		commandBeginning="";
		historyIndex=-1;
		
		textinput.addKeyListener(new KeyListener() {
			public void keyPressed(KeyEvent e) {	
				if (historyIndex == -1){
					commandBeginning=textinput.getText();
				}
				if(e.keyCode==16777217){//arrow_up
					if(historyIndex == -1){historyIndex++;}
					if(wrapper.getHistoryElement(historyIndex, commandBeginning) != null){
						textinput.setText(wrapper.getHistoryElement(historyIndex, commandBeginning));
						historyIndex++;
					}
					else{
						textinput.setText("");
						historyIndex=0;
					}
				}
				if(e.keyCode==16777218){//arrow_down
					if(wrapper.getHistoryElement(historyIndex -1, commandBeginning)!= null){
						textinput.setText(wrapper.getHistoryElement(historyIndex-1, commandBeginning));
						historyIndex--;
					}
					else{
						textinput.setText(commandBeginning);
						historyIndex=-1;
					}
				}
			}
			public void keyReleased(KeyEvent e) {}
		});
 
		textinput.addTraverseListener(new TraverseListener() {
			public void keyTraversed(TraverseEvent e) {
				if (e.detail == SWT.TRAVERSE_RETURN ) {
					//Prevent the return to set a newline into the text
					e.detail=SWT.TRAVERSE_NONE;
					e.doit=true;
					command = new String(textinput.getText());
		
					int charCount=textoutput.getCharCount();
					textoutput.append("-->"+command+"\n");
					StyleRange styleRange = new StyleRange();
					styleRange.start = charCount;
					styleRange.length = command.length()+3;
					styleRange.foreground = myColor;
					textoutput.setStyleRange(styleRange);
					
					getResult();

					textinput.setText("");
					textinput.forceFocus();
					
					commandBeginning="";
					historyIndex=-1;
				}
			}
		});
 
		DropTarget dt = new DropTarget(textinput, DND.DROP_DEFAULT | DND.DROP_MOVE );
		dt.setTransfer(new Transfer[] { FileTransfer.getInstance() });
		dt.addDropListener(new DropTargetAdapter() {
			public void drop(DropTargetEvent event) {
				String fileList[] = null;
				FileTransfer ft = FileTransfer.getInstance();
				if (ft.isSupportedType(event.currentDataType)) {
					fileList = (String[])event.data;
					textinput.setText("exec(\""+ fileList[0]+"\");");
				}
			}
		});
     
	}

	/**
	 * This method is a getter for the textInput text.
	 * @return the text in textInput.
	 * **/
	public String getInputText() {
		return textinput.getText();
	}
	
	/**
	 * This method is a setter for the textInput text.
	 * @param text is the text that is set in the textInput.
	 * **/
	public void setInputText(String text) {
		this.textinput.setText(text);
		textinput.forceFocus();
	}
	
	/**
	 * This method is a getter for the textOutput text.
	 * @return the text in textOutput.
	 * **/
	public String getOuputText() {
		return textoutput.getText();
	}
	
	/**
	 * This method is a setter for the textOutput text.
	 * @param text is the text that is set in the textOutput.
	 * **/
	public void setOuputText(String text) {
		this.textoutput.setText(text);
	}
	 
	/**
	 * This method is used to execute a command and show the result in the textOutput.
	 * The command is send to the JavaSciWrapper to executed.
	 * **/
	private void getResult() {
		outputVector = wrapper.exec(command);
		if(outputVector!=null){
			for(int i=0;i<outputVector.size();i++){
				textoutput.append((String)outputVector.get(i)+"\n");
			}
		}
		else{
			int charCount=textoutput.getCharCount();
			textoutput.append("error\n");
			StyleRange styleRange = new StyleRange();
			styleRange.start = charCount;
			styleRange.length = 5;
			styleRange.foreground = myRed;
			textoutput.setStyleRange(styleRange);
		}
		textoutput.setSelection(textoutput.getCharCount(), textoutput.getCharCount());
		textoutput.showSelection();
	}
	
	/**
	 * This method is part of the WrapperReceiver interface. This interface is implemented by every class that need the JavaSciWrapper then the plugin start.
	 * The plugin Activator uses this method when the JavaSciWrapper is loaded. It allows the plugin to load the visual components independently from the data which makes the plugin stable on startup.
	 * The javaSciWrapper is saved to be able to call its exec method then a command is executed.
	 * @param w is the javaSciWrapper that calls this method
	 * **/
	public void receiveWrapper(JavaSciWrapper w) {
		wrapper = w;
	}
 
}

