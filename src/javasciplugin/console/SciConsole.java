package javasciplugin.console;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorSite;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.part.EditorPart;

/**
 * This class defines the console editor of the Scilab plugin.
 * A view is a basic container for some visual content in eclipse. This view contains and manages an instance of the ConsoleWidget class.
 * **/
public class SciConsole  extends EditorPart{

	private ConsoleWidget console;

	/**
	 * This method is used by the plugin to register this editor on startup. It initialises the HistoryWidget and the Gridlayout.
	 * @param frame is the parent frame in which the widget is show.
	 * **/
	public void createPartControl(Composite frame) {
		console =new ConsoleWidget(frame, 0);
		GridData myGridData = new GridData(GridData.HORIZONTAL_ALIGN_FILL | GridData.GRAB_HORIZONTAL | GridData.VERTICAL_ALIGN_FILL | GridData.GRAB_VERTICAL);
		console.setLayoutData(myGridData);
		GridLayout myGridLayout=new GridLayout();
		console.setLayout(myGridLayout);
	}
	
	/**
	 * This method is executed when the SciHistory gets the focus.
	 * **/
	public void setFocus() {
		console.setFocus();
	}
	
	/**
	 * This method is executed when the plugin is closed.
	 * **/
	public void dispose() {
		console.dispose();
	}

	/**
	 * This method is a default eclipse method.
	 * **/
	public void doSave(IProgressMonitor monitor) {
	}
	
	/**
	 *This method is a default eclipse method.
	 * **/
	public void doSaveAs() {
	}

	/**
	 * This method is executed when the console is stated.
	 * @param site is the place on the workbench where the console will be shown.
	 * @param input is the input that the console takes.
	 * **/
	public void init(IEditorSite site, IEditorInput input) throws PartInitException {
		setSite(site);
        setInput(input);	
	}

	/**
	 * This method is a default eclipse method.
	 * **/
	public boolean isDirty() {
		return false;
	}

	/**
	 * This method is a default eclipse method.
	 * **/
	public boolean isSaveAsAllowed() {
		return false;
	}
	
	/**
	 * This method is used to send a command string to the console. It is used by the workspace view ,  file browser view and the history view.
	 * @param command is the command that is shown in the input of the console.
	 * **/
	public boolean execute(String command){
		console.setInputText(command);
		return true;
	}
}
