package javasciplugin.fileBrowser;


import java.io.File;
import java.util.Arrays;
import java.util.Comparator;

import javasciplugin.console.SciConsole;
import javasciplugin.console.myEditorInput;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.ToolBarManager;
import org.eclipse.jface.resource.ImageRegistry;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.events.TreeEvent;
import org.eclipse.swt.events.TreeListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.program.Program;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.DirectoryDialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeItem;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.ide.IDE;

/**
 * This class defines a SWT Widget that can be loaded as a part of a eclipse plugin.
 * It defines layout and the events of the Scilab file browser. It is composed of a SWT Tree and a ToolBar which are placed on a GridLayout.
 * **/
public class FileBrowserWidget extends Composite {
	private Display display;
	private Shell shell;
	private Tree tree;
	
	/**
	 * This constructor initialises the SWT components and puts them on a GridLayout.
	 * Furthermore the setRootDir action is added to the Toolbar and the tree listener, selection listener and default selection listener are added to the tree.
	 * The setRootDir action allows it to select a root directory from a Directory dialog for the tree.
	 * The tree listener reacts on tree expansion. When the tree expands it adds all the subfiles and the folder that are in the expanded folder to the tree.
	 * The selection listener sends the file path surrounded by "cp" command to the Console.
	 * The default selection listener sends the file path surrounded by exec("") command to the Console.
	 * @param parent is used to register the widget in the eclipse engine.
	 * @param style is used to register the widget in the eclipse engine.
	 * **/
	public FileBrowserWidget(Composite parent, int style) {
		super(parent, style);
		Action actionSetRootDir = new Action("Set Root Dir") {
			public void run() {
				DirectoryDialog dialog = new DirectoryDialog(shell);
				String path = dialog.open();
				if (path != null) {
					setRootDir(new File(path));
				}
			}
		};
		shell=this.getShell();
		display=this.getDisplay();
		
		ToolBar toolBar = new ToolBar(this, SWT.FLAT);
		ToolBarManager manager = new ToolBarManager(toolBar);
		manager.add(actionSetRootDir);		
		manager.update(true);
		tree = new Tree(this, SWT.BORDER);
		tree.setLayoutData(new GridData(GridData.FILL_BOTH));
		
		setRootDir(new File("/"));
		
		tree.addTreeListener(new TreeListener() {
			public void treeCollapsed(TreeEvent e) {
			}
			
			public void treeExpanded(TreeEvent e) {
				TreeItem item = (TreeItem) e.item;
				TreeItem[] children = item.getItems();
				for (int i = 0; i < children.length; i++)
					if (children[i].getData() == null)
						children[i].dispose();
					else
						return;
				
				File[] files = ((File) item.getData()).listFiles();
				filesSort(files);			
				for (int i = 0; files != null && i < files.length; i++)
					addFileToTree(item, files[i]);
			}
		});
		
		tree.addSelectionListener(new SelectionListener() {
			public void widgetSelected(SelectionEvent e) {
				TreeItem item = (TreeItem) e.item;
				File file = (File) item.getData();
				try {
					myEditorInput input=new myEditorInput();
					IEditorPart editorPart = IDE.openEditor(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage(), input,"javasciplugin.console.SciConsole");
					((SciConsole)editorPart).execute("cd "+file.getAbsolutePath());
				} catch (PartInitException e1) {
					e1.printStackTrace();
				}
			}
			
		public void widgetDefaultSelected(SelectionEvent e) {
			TreeItem item = (TreeItem) e.item;
			File file = (File) item.getData();
			try {
				myEditorInput input=new myEditorInput();
				IEditorPart editorPart = IDE.openEditor(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage(), input,"javasciplugin.console.SciConsole");
				((SciConsole)editorPart).execute("exec(\""+ file.getAbsolutePath()+"\");");
			} catch (PartInitException e1) {
				e1.printStackTrace();
			}
		}
		});
	}
	  
	
	/**
	 * This method resets the Tree and shows all the files and folders that are contained in the root folder.
	 * @param root is the root folder.
	 * **/
	private void setRootDir(File root) {
		if( (!root.isDirectory()) || (!root.exists()))
			throw new IllegalArgumentException("Invalid root: " + root);
		
		shell.setText("Now browsing: " + root.getAbsolutePath());
		if (tree.getItemCount() > 0) {
			TreeItem[] items = tree.getItems();
			for (int i = 0; i < items.length; i++) {
				items[i].dispose();
			}
		}
		
		File[] files = root.listFiles();
		filesSort(files);
		for(int i=0; files != null && i < files.length; i++)
			addFileToTree(tree, files[i]);
	}
	
	/**
	 *This method adds a file or folder to the Tree.
	 *@param parent is the parent TreeItem where the file should be added to
	 *@param file is the file that should be added. 
	 * **/
	private void addFileToTree(Object parent, File file) {
		TreeItem item = null;
		if (parent instanceof Tree)
			item = new TreeItem((Tree) parent, SWT.NULL);
		else if (parent instanceof TreeItem)
			item = new TreeItem((TreeItem) parent, SWT.NULL);
		else
			throw new IllegalArgumentException("Parent should be a tree or a tree item: " + parent);
		item.setText(file.getName());
		item.setImage(getIcon(file));
		item.setData(file);
		
		if (file.isDirectory()) {
			if (file.list() != null && file.list().length > 0)
				new TreeItem(item, SWT.NULL);
		}
	}
	
	/**
	 * This method is used to sort the array that contains all the files of a one folder.
	 * The array is sorted alphabetical no case sensitive and with the folders always before the files.
	 * @param files is the array of files contained in a folder.
	 * **/
	@SuppressWarnings({ "unused", "unchecked" })
	private void filesSort(File[] files){
		Arrays.sort(files,new Comparator(){
			public int compare(final Object o1, final Object o2) {
				File a =(File)o1;
				File b =(File)o2;
				if(a.isDirectory() && !b.isDirectory()){
					return -1;
				}
				if(b.isDirectory() && !a.isDirectory()){
					return 1;
				}
				return a.getName().compareToIgnoreCase(b.getName());
			}
		});
	}
	
	/**
	 * The ImageRegistry contains the icons to use with folders and with unknown file extentions.
	 * **/
	private ImageRegistry imageRegistry;
	Image iconFile = new Image(display, FileBrowserWidget.class.getResourceAsStream("unknown.gif"));
	Image iconFolder = new Image(display, FileBrowserWidget.class.getResourceAsStream("gnome-folder.png"));
	
	/**
	* Returns an icon representing the file given as input.
	* 
	* @param file
	* @return an icon representing the file given as input
	*/
	private Image getIcon(File file) {
		if (file.isDirectory())
			return iconFolder;
		int lastDotPos = file.getName().indexOf('.');
		if (lastDotPos == -1)
			return iconFile;	
		Image image = getIcon(file.getName().substring(lastDotPos + 1));
		return image == null ? iconFile : image;
	}
	
	/**
	* Returns an icon representing the file extension given as input.
	* 
	* @param extension
	* @return an icon representing the file extension given as input
	*/
	private Image getIcon(String extension) {
		if (imageRegistry == null)
			imageRegistry = new ImageRegistry();
		Image image = imageRegistry.get(extension);
		if (image != null)
			return image;
		
		Program program = Program.findProgram(extension);
		ImageData imageData = (program == null ? null : program.getImageData());
		if (imageData != null) {
			image = new Image(shell.getDisplay(), imageData);
			imageRegistry.put(extension, image);
		} else {
			image = iconFile;
		}
		return image;
	}
}