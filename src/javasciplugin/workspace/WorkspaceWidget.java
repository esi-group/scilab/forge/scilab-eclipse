package javasciplugin.workspace;

import java.util.Arrays;
import java.util.Comparator;
import java.util.Vector;

import javasciplugin.*;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;

/**
 * This class defines a SWT Widget that can be loaded as a part of a eclipse plugin.
 * It defines layout and the events of the Scilab workspace. It is a SWT Table which are placed on a GridLayout.
 * Furthermore a popup menu is used to show and hide columns.
 * **/
public class WorkspaceWidget  extends Composite implements Observer, WrapperReceiver {

	private Menu popupMenu;
	private MenuItem[] popupItems;
	private Table t;
	private TableColumn[] tc;
	
	private int numberOfColonms;
	private int size;
	private Vector workspaceVector;
	private String[][] data;

	/**
	 * This method hides and shows the columns that are selected in the popup menu. 
	 * If a column is check in the menu it is show else its size is set to 0 and resizable is set to false.
	 * @param item is the menuItem that was checked or unchecked.
	 * **/
	private void refreshMenu(MenuItem item){
		if (item != null){
    		String text = item.getText();
    		int i=0;
    		if (text.compareTo("Name")==0){i=0;}
    		if (text.compareTo("Value")==0){i=1;}
    		if (text.compareTo("Type")==0){i=2;}
    		if (text.compareTo("Size")==0){i=3;}


			if(!item.getSelection()){
				tc[i].setWidth(0);
				tc[i].setResizable(false);
			}
			else{
				tc[i].setResizable(true);
			}
			
			int countColomns = 0;
	    	for(int j=0;j<4;j++){
	    		if(tc[j].getResizable()){countColomns++;}
			}
			if(countColomns==0){countColomns=1;}
			int width= t.getSize().x/countColomns;
			
			for(int j=0;j<4;j++){
				if(tc[j].getResizable()){
					tc[j].setWidth(width);
				}
			}

    	}
	}

	/**
	 * This method refreshes the data of the workspace. The workspace contains the active Scilab variables by there name, type, size and there first value.
	 * These data are set in the data array which is show on every redraw of the table.
	 **/ 
	private void refreshData(){
		t.clearAll();	
		size = workspaceVector.size()/4;
		data = new String [size][numberOfColonms];
		for(int i=0;i<size;i++){
			for(int j=0;j<numberOfColonms;j++){
				data[i][j]=(String)workspaceVector.get(i*4+j);
			}
		}
		t.setItemCount(size);
		t.redraw();
	}
	
	/**
	 * This constructor initialises the SWT components and puts them on a GridLayout.
	 * Furthermore the listener on the popup menu, on resize of the table, on setData of the table and on the sort of the table are set.
	 * The popup menu is shown on right click in the workspaceWidget. It contains a list of the different columns that the workspace table contains.
	 * If the columns is check in the popup menu than it is shown. If it is unchecked the respective column is hidden.
	 * The resize listener reacts on resize of the widget. It divides the widget's width among all the column.
	 * The setData listener set the data from the data array in the Table.
	 * The sort array reacts on the sorting of the columns which is done by clicking on the title of the column.   
	 * Furthermore the javaSciWrapper is requested from the Activator.
	 * @param parent is used to register the widget in the eclipse engine.
	 * @param style is used to register the widget in the eclipse engine.
	 * **/
	public WorkspaceWidget(Composite parent, int style) {
	    super(parent, style);

	    workspaceVector=new Vector<String>();	    
	    numberOfColonms = 4;
		size = workspaceVector.size()/4;
		data = new String [size][numberOfColonms];
		
	    Activator.getDefault().getWrapper(this);
		
		t = new Table(this, SWT.VIRTUAL);		
		GridData MyGridData=new GridData(GridData.FILL_BOTH);
		t.setLayoutData(MyGridData); 
		t.setItemCount(size);
		Font myFont= new Font(this.getParent().getDisplay(), "Arial", 8, SWT.NORMAL);
		t.setFont(myFont);
		
		tc = new TableColumn[numberOfColonms];
		tc[0] = new TableColumn(t, SWT.LEFT);		
		tc[1] = new TableColumn(t, SWT.LEFT);
		tc[2] = new TableColumn(t, SWT.LEFT);
		tc[3] = new TableColumn(t, SWT.LEFT);
		
		tc[0].setText("Name");
		tc[1].setText("Value");
		tc[2].setText("Type");
		tc[3].setText("Size");
		
		tc[0].setWidth(100);
		tc[1].setWidth(100);
		tc[2].setWidth(100);
		tc[3].setWidth(100);
		
		tc[0].pack();
		tc[0].setMoveable(true);
		tc[1].pack();
		tc[1].setMoveable(true);;
		tc[2].pack();
		tc[2].setMoveable(true);
		tc[3].pack();
		tc[3].setMoveable(true);
		
		t.setHeaderVisible(true);

		Listener sortListener = new Listener() {			
			public void handleEvent(Event e) {
				TableColumn sortColumn = t.getSortColumn();
				TableColumn currentColumn = (TableColumn)e.widget;
				int dir = t.getSortDirection();
				if (sortColumn == currentColumn) {
					dir = dir == SWT.UP ? SWT.DOWN : SWT.UP;
				} else {
					t.setSortColumn(currentColumn);
					dir = SWT.UP;
				}
				t.setSortDirection(dir);
				tableSort();
			}
		};
		
		tc[0].addListener(SWT.Selection, sortListener);
		tc[1].addListener(SWT.Selection, sortListener);
		tc[2].addListener(SWT.Selection, sortListener);
		tc[3].addListener(SWT.Selection, sortListener);
		t.setSortColumn(tc[0]);
		t.setSortDirection(SWT.UP);
		t.clearAll();
		
		popupMenu = new Menu(this.getShell(), SWT.POP_UP);
		popupItems=new MenuItem[numberOfColonms];
		for(int i=0;i<numberOfColonms;i++){
    		popupItems[i] = new MenuItem(popupMenu, SWT.CHECK);
    		popupItems[i].setSelection(true);
    		popupItems[i].addSelectionListener(new SelectionAdapter() {
    	        public void widgetSelected(SelectionEvent event) {
    	        	MenuItem item = (MenuItem)event.widget;
    	        	refreshMenu(item);	            		
    	        }
    	     });
		}
    	popupItems[0].setText("Name");
    	popupItems[1].setText("Value");
    	popupItems[2].setText("Type");
    	popupItems[3].setText("Size");
	    
    	t.addListener(SWT.Resize, new Listener(){
			public void handleEvent(Event e) {
				for(int i= 0;i< numberOfColonms;i++){
					refreshMenu(popupItems[i]);
            	}	
			}
		});
    	
	    t.addListener(SWT.MenuDetect, new Listener() {
	        public void handleEvent(Event event) {
	          popupMenu.setLocation(event.x, event.y);
	          popupMenu.setVisible(true);
	        }
	      });
	    
	    t.addListener(SWT.SetData, new Listener() {
			public void handleEvent(Event e) {
				TableItem item = (TableItem) e.item;
				int index = t.indexOf(item);
				String[] cols =(String[]) data[index];
				item.setText(new String[] {cols[0], cols[1], cols[2], cols[3] });
			}
		});
	 
	}
	
	/**
	 * This method is used to sort the table.
	 * **/
	@SuppressWarnings("unchecked")
	private void tableSort(){

		TableColumn currentColumn = t.getSortColumn();
		int dir = t.getSortDirection();
		
		final int index;
		if(currentColumn == tc[0]){
			index = 0;
		}else if(currentColumn == tc[1]){
			index = 1;
		}else if(currentColumn == tc[2]){
			index = 2;
		}else{
			index = 3;
		}
		final int direction = dir;
		
		Arrays.sort(data, new Comparator() {
			public int compare(final Object arg0, final Object arg1) {
					final String[] a = (String[]) arg0;
					final String[] b = (String[]) arg1;
					if(a!=null && b!=null){
						if (direction == SWT.UP) {
							return a[index].toLowerCase().compareTo(b[index].toLowerCase());	
						}
						else{
							return -1 * (a[index].toLowerCase().compareTo(b[index].toLowerCase()));
						}
					}
					else{
						return 0;
					}
			}
		});
		t.clearAll();
	}

	/**
	 * This method is called by the WrapperReceiver method. It inserts the variables in the table, starts the console and adds this widget as an Observer to the JavaSciWrapper.
	 * Now on every command execution the workspace is updated.
	 * @param w is the javaSciWrapper that calls the WrapperReceiver method
	 * **/
	private void initWorkspace(JavaSciWrapper w){
		workspaceVector = w.getSciWorkspace();
		this.refreshData();
		tableSort();
		this.redraw();
		w.addObserver(WorkspaceWidget.this);
		Activator.getDefault().startConsole();
	}

	/**
	 * This method is part of the WrapperReceiver interface. This interface is implemented by every class that need the JavaSciWrapper during the start of the plugin.
	 * The plugin Activator uses this method when the JavaSciWrapper is loaded. This allows the plugin to load the visual independent from the data.
	 * This makes the plugin stable on startup.
	 * @param w is the javaSciWrapper that calls this method
	 * **/
	public void receiveWrapper(final JavaSciWrapper w) {
		Display.getDefault().asyncExec(new Runnable() {

			public void run() {
				WorkspaceWidget.this.initWorkspace(w);
			}
		});
	}
	
	/**
	 * This method is part of the Observer interface. This interface is implemented by every class that the JavaSciWrapper to notify command executions.
	 * The consequence of a command execution is that the workspace might have changed and need to be updated.
	 * @param pWrapper is the javaSciWrapper that calls this method
	 * **/
	public void update(JavaSciWrapper pWrapper) {
		workspaceVector = pWrapper.getSciWorkspace();
		this.refreshData();
		tableSort();
		this.redraw();
	}
}

